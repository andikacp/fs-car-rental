const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = {
    mode: 'development',
    target: 'node',
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        clean: true,
        filename: 'server.bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                loader: 'css-loader',
            },
        ]
    },
    externals: [
        nodeExternals(),
    ]
}