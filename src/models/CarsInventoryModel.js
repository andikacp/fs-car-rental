import { DataTypes } from 'sequelize';
import { Database } from '../libraries/database';

function CarsInventory() {
    return Database.define('CarsInventory', {
        //.define('namaperwakilanTabel', {} -> kolom di tabel, {} -> konfigurasi table di postgre)
        ID: {
            type: DataTypes.NUMBER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        Brand: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        Variant: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        Qty: {
            type: DataTypes.NUMBER,
            allowNull: false,
        },
        Year: {
            type: DataTypes.STRING,
            allowNull: false,
        }
    }, {
        tableName: 'tb_car_inventory',
        timestamps: false,  //defaultnya true, ini didisable dulu buat prevent error    
    });
}

export default CarsInventory;
