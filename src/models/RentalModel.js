import { DataTypes } from 'sequelize';
import { Database } from '../libraries/database';

function Rental(){
    return Database.define('Rental',{
        ID: {
            type: DataTypes.INTEGER,
            allownull: false,
            primaryKey: true,
            autoIncrement: true,
          },
    
          CarID:{
            type: DataTypes.INTEGER,
            allownull: false,
          },
    
          ReturnDate:{
            type: DataTypes.DATE,
            allownull: false,
          },
    
          IsReturned: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
          },
    
          WithDriver: {
            type: DataTypes.BOOLEAN,
            defaultValue: false,
          },
    
          CustomerID: {
            type: DataTypes.INTEGER,
            allowNull: false,
          },
    },
    {
      tableName: 'tb_rental',
      timestamps: true,
    });
}

export default Rental;