import { DataTypes } from 'sequelize';
import { Database } from '../libraries/database';

function Cars(){
    return Database.define('Cars',{
        ID: {
            type: DataTypes.NUMBER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        PoliceNumber: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        Price: {
            type: DataTypes.NUMBER,
            allowNull: false,
        },
        Inventory_ID: {
            type: DataTypes.NUMBER,
            allowNull: false,
        }

    }, {
        tableName: 'tb_car',
        timestamps: false,

    });
}

export default Cars;