import { DataTypes } from 'sequelize';
import { Database } from '../libraries/database';

function Payment(){
     //.define('namaperwakilanTabel', {} -> kolom di tabel, {} -> konfigurasi table di postgre)
     return Database.define('Payment', {
        ID: {
            type: DataTypes.NUMBER,
            allownull: false,
            primaryKey: true,
            autoIncrement: true,
          },
    
          RentID: {
            type: DataTypes.NUMBER,
            allownull: false,
          },
    
          PaymentType: {
            type: DataTypes.STRING,
            allownull: false,
          },
    
          Amount: {
            type: DataTypes.NUMBER,
            allownull: false,
          },
    
          PaymentMethod: {
            type: DataTypes.STRING,
            allownull: false,
          },
    
          IsVerified: {
            type: DataTypes.BOOLEAN,
            allownull: false,
          },
    
          StaffID:{
            type: DataTypes.NUMBER,
            allownull: false,
          },
     }, {
         tableName: 'tb_payment',
         timestamps: false,
     });
}

export default Payment;