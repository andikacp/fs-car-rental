import { DataTypes } from 'sequelize';
import { Database } from '../libraries/database';

function Customer(){
    return Database.define('Customer',{
        ID: {
            type: DataTypes.NUMBER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        ID_Number: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        Name: {
            type: DataTypes.STRING,
            allowNull:false,
        },
        Address: {
            type: DataTypes.TEXT,
            allowNull: false,
        },
        Phone: {
            type: DataTypes.STRING,
            allowNull:false,
        }

    }, {
        tableName: 'tb_customer',
        timestamps: false,
    });
}

export default Customer;