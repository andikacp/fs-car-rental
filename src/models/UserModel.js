import { DataTypes } from 'sequelize';
import { Database } from '../libraries/database';

function User() {
    // .define('namaPerwakilanTabel', {} -> kolom tabel di database, {} -> konfigurasi / mapping ke tabel apa di Postgre )
    return Database.define('User', {
        ID: {
            type: DataTypes.NUMBER, // character variying , string
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        Username: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        Password: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        Email: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        FullName: {
            type: DataTypes.STRING,
            allowNull: false,
        },
    }, {
        tableName: 'tb_user',
        timestamps: true, // 
    });
}

export default User;
