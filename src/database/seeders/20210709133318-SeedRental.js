'use strict';
const moment = require('moment');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('tb_rental', [
      {
        CarID: 1,
        ReturnDate: moment().add(7, 'days').toDate(),
        IsReturned: false,
        WithDriver: false,
        CustomerID: 1,
        createdAt: moment().toDate(),
        updatedAt: moment().toDate(),
      },
      {
        CarID: 2,
        ReturnDate: moment().add(14, 'days').toDate(),
        IsReturned: false,
        WithDriver: false,
        CustomerID: 1,
        createdAt: moment().toDate(), //otomatis ambil tanggal skg
        updatedAt: moment().toDate(),
      },
      {
        CarID: 3,
        ReturnDate: moment().add(5, 'days').toDate(),
        IsReturned: false,
        WithDriver: false,
        CustomerID: 2,
        createdAt: moment().toDate(), //otomatis ambil tanggal skg
        updatedAt: moment().toDate(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
