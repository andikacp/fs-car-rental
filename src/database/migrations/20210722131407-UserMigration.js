'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.createTable('tb_payment',{
      ID: {
        type: Sequelize.INTEGER,
        allownull: false,
        primaryKey: true,
        autoIncrement: true,
      },

      RentID: {
        type: Sequelize.INTEGER,
        allownull: false,
      },

      PaymentType: {
        type: Sequelize.STRING,
        allownull: false,
      },

      Amount: {
        type: Sequelize.INTEGER,
        allownull: false,
      },

      PaymentMethod: {
        type: Sequelize.STRING,
        allownull: false,
      },

      IsVerified: {
        type: Sequelize.BOOLEAN,
        allownull: false,
      },

      StaffID:{
        type: Sequelize.INTEGER,
        allownull: false,
      },
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
