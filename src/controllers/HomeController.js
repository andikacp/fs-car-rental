'use-strict';

import UserModel from '../models/UserModel';
import CarModel from '../models/CarsModel';
import CarInventoryModel from '../models/CarsInventoryModel';
class HomeController {
    async index(req, res) {
        try{
            const carModel = CarModel();
            const carInventoryModel = CarInventoryModel();
            carModel.belongsTo(carInventoryModel, {foreignKey: 'Inventory_ID', as: 'Inventory'});
            const data = await carModel.findAll({
                include: [
                    {
                        model: carInventoryModel,
                        as: 'Inventory',
                    }
                ]
            });
            // console.log('result::', data[0]);
            res.render('pages/home',{
                cars: data,
            });
            // res.json(data);
        }

        catch(error){
            console.log(error.toString());
            res.send('error');
        }
    }

    about(req, res) {
        res.render('pages/about');
    }

    account(req, res) {
        return res.render('pages/account', {
            user: req.user
        });
    }
}

export default new HomeController();
