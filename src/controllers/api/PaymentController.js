import PaymentModel from "../../models/PaymentModel";
import RentalModel from "../../models/RentalModel"
import { makeResponse } from "../../libraries/response";


class PaymentController{
    async getAllPayments(req,res){
        try {
            const model = PaymentModel();
            const data = await model.findAll();

            makeResponse(res, {
                code: 200,
                message: 'successfully fetched all payments ',
                data,
            })
        } catch (error) {
            makeResponse(res,{
                code: 500,
                message: 'failed fetching all payments',
                data: {},
            });
        }
    }

    async addPayment(req,res){
        const model = PaymentModel();
        const rentalModel = RentalModel();
        const data = req.body;
        const user = req.user;
        
        if(data.PaymentType == "lunas" || data.PaymentType=="cicil"){
            if(data.PaymentMethod == "cash" || data.PaymentMethod == "transfer" || data.PaymentMethod == "credit card"  ){
                try {
                    const rental = await rentalModel.findOne({
                        where: {
                            ID: data.RentID,
                        }
                    });
                    const create = await model.create({
                        RentID: rental.RentID,
                        PaymentType: data.PaymentType,
                        PaymentMethod: data.PaymentMethod,
                        Amount: data.Amount,
                        IsVerified: true,
                        StaffID: user.ID,
                    });
                    makeResponse(res, {
                        code: 201,
                        message: "Payment success!",
                        data: create,
                    })
                } catch (error) {
                    makeResponse(res, {
                        code: 404,
                        message: "Rent record not found",
                        data: {},
                    })
                }
                
            }
            else{
                makeResponse(res,{
                    code: 406,
                    message: "Payment method not allowed. Please enter 'cash' or 'transfer' or'credit card'",
                    data: {},
                });    
            }
        }
        else {
            makeResponse(res,{
                code: 406,
                message: "Payment type not allowed. Please enter 'lunas' or 'cicil'",
                data: {},
            });
        }
        

    }
}

export default new PaymentController;