class ApiController {
    getBooks(req, res) {
        res.send({
            data: [
                {
                    id: '1',
                    author: 'John',
                    title: 'Book One',
                    year: '2021'
                },
                {
                    id: '2',
                    author: 'John',
                    title: 'Book Two',
                    year: '2021'
                },
                {
                    id: '3',
                    author: 'John',
                    title: 'Book Three',
                    year: '2021'
                },
            ]
        })
    }
}

export default new ApiController();
