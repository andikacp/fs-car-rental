import CarsModel from "../../models/CarsModel";
import { makeResponse } from "../../libraries/response";

class CarsController{

    //select all
    async getAllCarDetails(req,res) {
        try {
            const model = CarsModel();
            const data = await model.findAll();

            makeResponse(res, {
                code: 200,
                message: 'success fetching all car details',
                data,
            });
        } catch (error){
            makeResponse(res, {
                code: 500,
                message: 'failed fetching all car details',
                data: {},
            })
        }
    }

    //insert into table
    async addCarDetails(req,res){
        const model = CarsModel();
        const data = req.body;
        const create = await model.create({
            PoliceNumber: data.PoliceNumber,
            Price: data.Price,
            Inventory_ID: data.Inventory_ID,
        });

        makeResponse(res, {
            code: 201,
            message: 'success',
            data: create,
        });
    }

    //update table
    async updateCarDetails(req,res){
        const model = CarsModel();
        const id = req.params.id;
        const data = req.body;

        const update = await model.update({
            PoliceNumber: data.PoliceNumber,
            Price: data.Price,
            Inventory_ID: data.Inventory_ID,
        }, {
            where: {
                ID : id,
            },
        });

        makeResponse(res, {
            code: 200,
            message: 'success updating car details',
            data: update,
        });
    }

    //remove car details
    async removeCarDetails(req, res) {
        const model = CarsModel();
        const id = req.params.id;

        const remove = await model.destroy({
            where: {
                ID: id,
            }
        });

        makeResponse(res, {
            code: 200,
            message: 'success remove cars',
            data: remove
        });
    }

    //get by id
    async getCarDetailsById(req, res) {
        const model = CarsModel();
        const id = req.params.id;

        const data = await model.findOne({ where: { ID: id } });

        makeResponse(res, {
            code: 200,
            message: 'success getting car details',
            data,
        });
    }
}

export default new CarsController();