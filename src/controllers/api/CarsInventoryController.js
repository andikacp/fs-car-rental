import CarsInventoryModel from "../../models/CarsInventoryModel";
import { makeResponse } from "../../libraries/response";

class CarsInventoryController {
    async getAllCars(req, res) {
        try {
            const model = CarsInventoryModel();
            const data = await model.findAll();

            makeResponse(res, {
                code: 200,
                message: 'success fetch all cars inventory',
                data,
            });
        } catch (error) {
            makeResponse(res, {
                code: 500,
                message: 'failed fetch all cars',
                data: {},
            })
        }
    }

    async addCars(req, res) {
        const model = CarsInventoryModel();
        const data = req.body;
        const create = await model.create({
            Brand: data.Brand,
            Variant: data.Variant,
            Qty: data.Qty,
            Year: data.Year,
        });

        makeResponse(res, {
            code: 201,
            message: 'success',
            data: create,
        });
    }

    async updateCars(req, res) {
        const model = CarsInventoryModel();
        const id = req.params.id;
        const data = req.body;

        const update = await model.update({
            Brand: data.Brand,
            Variant: data.Variant,
            Qty: data.Qty,
            Year: data.Year,
        }, {
            where: {
                ID: id,
            },
        });

        makeResponse(res, {
            code: 200,
            message: 'success update cars inventory',
            data: update,
        });
    }

    async removeCars(req, res) {
        const model = CarsInventoryModel();
        const id = req.params.id;

        const remove = await model.destroy({
            where: {
                ID: id,
            }
        });

        makeResponse(res, {
            code: 200,
            message: 'success remove cars',
            data: remove
        });
    }

    async getCarById(req, res) {
        const model = CarsInventoryModel();
        const id = req.params.id;

        const data = await model.findOne({ where: { ID: id } });

        makeResponse(res, {
            code: 200,
            message: 'success get single cars',
            data,
        });
    }
}

export default new CarsInventoryController();