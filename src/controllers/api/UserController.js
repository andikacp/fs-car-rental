import { makeResponse } from '../../libraries/response';
import UserModel from '../../models/UserModel';
import Password from '../../libraries/password';
import Token from '../../libraries/token';

class UserController {
    constructor(userModel) {
        this._userModel = userModel;
    }

    async registration(req, res) {
        try {
            const {
                Username,
                Email,
                FullName
            } = req.body;
            const userModel = UserModel();
            const data = await userModel.create({
                Username,
                Email,
                FullName,
                Password: Password.makePassword(req.body.Password),
            });
            makeResponse(res, {
                code: 201,
                message: 'success create user',
                data,
            });
        } catch (error) {
            makeResponse(res, {
                code: 500,
                message: 'failed create user',
                data: error.toString(),
            });
        }
    }

    async login(req, res) {
        try {
            const userModel = UserModel();
            const data = await userModel.findOne({
                where: {
                    Username: req.body.Username,
                }
            });
            if (data) {
                if (Password.matchPassword(req.body.Password, data.Password)) {

                    // create access token
                    const token = Token.sign({
                        ID: data.ID,
                        Username: data.Username,
                        Email: data.Email,
                        FullName: data.FullName,
                    });

                    return makeResponse(res, {
                        code: 200,
                        message: 'successfully logged in',
                        data: {
                            accessToken: token,
                        },
                    });
                }
                return makeResponse(res, {
                    code: 403,
                    message: 'Username and Password is failed',
                    data: {},
                });
            }

            return makeResponse(res, {
                code: 403,
                message: 'Login failed',
                data: {},
            });

        } catch (error) {
            makeResponse(res, {
                code: 500,
                message: 'Login failed',
                data: error.toString(),
            });
        }
    }

    async profile(req, res) {
        try {
            const userModel = UserModel();
            const user = req.user;

            const data = await userModel.findOne({
                where: {
                    ID: user.ID,
                },
                attributes: {
                    exclude: ['Password'] // field password tidak perlu ditampilkan pada response
                }
            });

            makeResponse(res, {
                code: 200,
                message: 'success fetch user profile',
                data,
            });

        } catch (error) {
            makeResponse(req, {
                code: 500,
                message: 'Failed get user profile',
                data: {},
            });
        }
    }
}

export default UserController;
