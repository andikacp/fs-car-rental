import RentalModel from '../../models/RentalModel';
import { makeResponse } from '../../libraries/response';
const moment = require('moment');

class RentalController {
    async getAllRental(req, res) {
        try {
            const model = RentalModel();
            const data = await model.findAll();

            makeResponse(res, {
                code: 200,
                message: 'successfully fetching all rent data',
                data,
            });
        } catch (error) {
            makeResponse(res, {
                code: 500,
                message: 'failed fetching all rent data',
                data: {},
            });
        }
    }

    async addRental(req,res){
        try {
            const payload = req.body;
            const model = RentalModel();
            const response = await model.create({
                CarID: payload.CarID,
                CustomerID: payload.CustomerID,
                ReturnDate: moment().add(payload.ReturnDate, 'days').toDate(),
                IsReturned: payload.IsReturned,
                WithDriver: payload.WithDriver,
                createdAt: moment().toDate(),
                updatedAt: moment().toDate(),
            });
            return res.status(201).json({
                message: "successfully added a car to your rental request!",
                data: response,
            });
            
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

    async removeRental(req, res) {
        try {
            const rentalID = req.params.rentalID;
            const model = RentalModel();
            const response = await model.destroy({
                where: {
                    ID: rentalID,
                }
            });
            return res.status(200).json({
                data: response,
                message: "Successfully removed rental request!"
            });
        } catch (error) {
            return res.status(500).json({
                error: error.toString(),
            });
        }
    }

}

export default new RentalController;