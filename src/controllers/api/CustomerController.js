import CustomerModel from "../../models/CustomerModel";
import { makeResponse } from "../../libraries/response";

class CustomerController{

     //select all
     async getCustomers(req,res) {
        try {
            const model = CustomerModel();
            const data = await model.findAll();

            makeResponse(res, {
                code: 200,
                message: 'success fetching all customers',
                data,
            });
        } catch (error){
            makeResponse(res, {
                code: 500,
                message: 'failed fetching all customers',
                data: {},
            })
        }
    }

    async addCustomer(req,res){
        const model = CustomerModel();
        const data = req.body;
        const create = await model.create({
            ID_Number: data.ID_Number,
            Name: data.Name,
            Address: data.Address,
            Phone: data.Phone,
        });

        makeResponse(res, {
            code: 201,
            message: 'success',
            data: create,
        });
    }

      //update table
      async updateCustomer(req,res){
        const model = CustomerModel();
        const id = req.params.id;
        const data = req.body;

        const update = await model.update({
            ID_Number: data.ID_Number,
            Name: data.Name,
            Address: data.Address,
            Phone: data.Phone,
        }, {
            where: {
                ID : id,
            },
        });

        makeResponse(res, {
            code: 200,
            message: 'success updating customer details',
            data: update,
        });
    }

    //remove customer
    async removeCustomer(req, res) {
        const model = CustomerModel();
        const id = req.params.id;

        const remove = await model.destroy({
            where: {
                ID: id,
            }
        });

        makeResponse(res, {
            code: 200,
            message: 'success removing customer',
            data: remove
        });
    }

    //get by id
      async getCustomerByID(req, res) {
        const model = CustomerModel();
        const id = req.params.id;

        const data = await model.findOne({ where: { ID: id } });

        makeResponse(res, {
            code: 200,
            message: 'success getting customer details',
            data,
        });
    }
}

export default new CustomerController();