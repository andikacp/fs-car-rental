import Password from '../libraries/password';
import Session, { SessionFlash } from '../libraries/session';

import UserModel from '../models/UserModel';
class AuthController {
    login(req, res) {
        const csrfToken = req.getCsrfToken();
        const flashMessage = SessionFlash.getFlashMessage(req);
        res.render('pages/login', {
            csrfToken,
            ...flashMessage,
        });
    }

    async doLogin(req, res) {
        SessionFlash.clearFlashMessage(req, res);
        try {
            const userModel = UserModel();
            const { username } = req.body;
            const data = await userModel.findOne({
                where: {
                    Username: username,
                }
            });
            if (data) {
                // validate password
                if (Password.matchPassword(req.body.password, data.Password)) {
                    await Session.makeSession(res, data.ID);
                    return res.redirect('/account');
                }
            }

            SessionFlash.flashMessage(res, { message: 'Username or Password is incorrect', type: 'error' });
            return res.redirect('/login');
        } catch (error) {
            res.send(error.toString());
        }
    }

    async doLogout(req, res) {
        try {
            await Session.clearSession(req, res);
            res.redirect('/login');
        } catch (error) {
            res.send(error.toString());
        }
    }
}

export default new AuthController();
