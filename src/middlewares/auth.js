/*export default function (req, res) {

}
*/
import { makeResponse } from '../../libraries/response';
import Token from '../../libraries/token';

/**
 * 
 * @param {} headers ['authorization'] 
 * @returns boolean 
 */
export const hasHeaderAuthorization = (headers) => {
    return headers && headers['authorization'];
};

/**
 * 
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @returns next or response failed
 */
export const validateToken = (req, res, next) => {
    try {
        if (hasHeaderAuthorization(req.headers)) {
            Token.setToken = req.headers['authorization'];
            req.user = Token.decoded();
            return next();
        }

        makeResponse(res, {
            code: 403,
            message: 'Unauthorized',
            data: 'Headers authorization not found',
        });
    } catch (error) {
        makeResponse(res, {
            code: 403,
            message: 'Unauthorized',
            data: error.toString(),
        });
    }
};
