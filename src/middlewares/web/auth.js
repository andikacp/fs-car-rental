import Session from '../../libraries/session';

// without session : redirect to login
export const withoutSession = async (req, res, next) => {
    try {
        if (Session.hasSession(req)) {
            // parsing cookies
            const user = await Session.getUser(req);

            Object.assign(req, {
                user,
            });

            return next();
        }

        res.redirect('/login');
    } catch (error) {
        res.send(error.toString());
    }
};

// withsession : redirect to account page
export const withSession = async (req, res, next) => {
    if (Session.hasSession(req)) {
        return res.redirect('/account');
    }
    next();
};
