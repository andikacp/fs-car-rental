import { Sequelize } from 'sequelize';

export const Database = new Sequelize({
    dialect: 'postgres',
    database: 'db_car_rental',
    username: 'postgres',
    password: 'admin'
});

// check database connections
export async function checkConnection() {
    console.log('trying to connect postgre database...');
    try {
        await Database.authenticate();
        console.log('Database connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database ', error);
        throw (error);
    }
}

export default Database;
