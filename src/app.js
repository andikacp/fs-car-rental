"use-strict";

import express from 'express';
import bodyParser from 'body-parser';
import ejs from 'ejs';
import cors from 'cors';
import dotenv from 'dotenv';

import web from './routes/web'; // di akses melalui browser
import api from './routes/api'; // di akses melalui Postman

import { checkConnection } from './libraries/database';

import Password from './libraries/password';

dotenv.config(); // initialize .env file

(async function () {
    await checkConnection();

    const app = express();

    const jsonParser = bodyParser.json(); // req.body -> json
    const urlEncoded = bodyParser.urlencoded({ extended: false }); // req.body -> Form 

    app.use(cors('*'));

    app.set('views', './src/views');
    app.set('view engine', 'ejs');
    app.engine('.ejs', ejs.__express);

    app.use(jsonParser);
    app.use(urlEncoded);

    app.use(function (req, res, next) {
        // csrf
        const token = Password.generateCsrf();
        req.headers['x-csrf-token'] = token;
        req.getCsrfToken = () => token;

        next();
    }, web()); // url / -> prefix /
    app.use('/api/v1', api()); // url /api/v1 -> prefix /api/v1
    app.use('/static', express.static('./src/public')); // /static -> menyajikan file statis (css, js, image dll)

    app.use(function (req, res, next) {
        res.status(404).send('Not Found');
    });

    app.use(function (err, req, res, next) {
        if (err) {
            console.error(err.stack);
        }

        res.status(500).send('Something broke!');
    });

    app.listen(3000, function () {
        console.log('running server on port http://localhost:3000');
    });
})();
