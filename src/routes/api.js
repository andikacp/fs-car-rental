'use-strict';

import express from 'express';

// load controllers
import CarsInventoryController from '../controllers/api/CarsInventoryController';
import CarsController from '../controllers/api/CarsController';
import RentalController from '../controllers/api/RentalController';
import UserController from '../controllers/api/UserController';
import PaymentController from '../controllers/api/PaymentController';

// load model
import UserModel from '../models/UserModel';
import { validateToken } from '../middlewares/api/auth';

export default function () {
    const router = express.Router();

    const userController = new UserController(UserModel);

    router.post('/user/registration', function (req, res) {
        userController.registration(req, res);
    });

    router.post('/user/login', function (req, res) {
        userController.login(req, res);
    });

    // tambahkan middleware validateToken
    router.get('/user/profile', validateToken, function (req, res) {
        userController.profile(req, res);
    });

    router.get('/cars', function (req, res) {
        CarsController.getAllCarDetails(req, res);
    });

    router.post('/cars', function (req, res) {
        CarsController.addCarDetails(req, res);
    });

    router.get('/cars/:id', function (req, res) {
        CarsController.getCarDetailsById(req, res);
    });

    router.put('/cars/:id', function (req, res) {
        CarsController.updateCarDetails(req, res);
    });

    router.delete('/cars/:id', function (req, res) {
        CarsController.r(req, res);
    });

    // REST API
    // /api/v1/cars/inventory -> GET -> Perwakilan dari API getAllCars
    router.get('/inventory', function (req, res) {
        CarsInventoryController.getAllCars(req, res);
    });

    // /api/v1/cars/inventory -> POST -> Perwakilan dari API addCars
    router.post('/inventory', function (req, res) {
        CarsInventoryController.addCars(req, res);
    });

    // /api/v1/cars/inventory/2 -> GET Perwakilan dari API getCarsById
    router.get('/inventory/:id', function (req, res) {
        CarsInventoryController.getCarsById(req, res);
    });

    // /api/v1/cars/inventory/2 -> PUT Perwakilan dari API updateCars
    router.put('/inventory/:id', function (req, res) {
        CarsInventoryController.updateCars(req, res);
    });

    // /api/v1/cars/inventory/2 -> DELETE Perwakilan dari API removeCars
    router.delete('/inventory/:id', function (req, res) {
        CarsInventoryController.removeCars(req, res);
    });

    //--------RENTAL--------------
    router.get('/rental', validateToken, function (req, res) {
        RentalController.getAllRental(req, res);
    });

    router.post('/rental', validateToken, function(req,res){
        RentalController.addRental(req,res);
    });

    router.delete('/rental/:rentalID', validateToken, function(req,res){
        RentalController.removeRental(req,res);
    });


    //--------PAYMENT----------
    router.post('/payment', validateToken, function(req,res){
        PaymentController.addPayment(req,res);
    });

    router.get('/payment', validateToken, function(req,res){
        PaymentController.getAllPayments(req,res);
    });
    return router;

}
