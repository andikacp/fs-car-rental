'use-strict';

import express from 'express';

// load controllers
import HomeController from '../controllers/HomeController';
import AuthController from '../controllers/AuthController';

import {
    withSession,
    withoutSession,
} from '../middlewares/web/auth';

export default function () {
    const router = express.Router();

    // home controller
    router.get('/', makeRouter(HomeController.index));
    router.get('/about', makeRouter(HomeController.about));

    // auth controller
    router.get('/login', withSession, makeRouter(AuthController.login));
    router.post('/login', withSession, makeRouter(AuthController.doLogin));
    router.get('/logout', withoutSession, makeRouter(AuthController.doLogout));
    router.get('/account', withoutSession, makeRouter(HomeController.account));

    return router;
}

function makeRouter(apiController) {
    return function (req, res) {
        apiController(req, res);
    }
}
